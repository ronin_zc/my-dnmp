<?php

$dir = __DIR__;
$path = $dir . '/greenvalley/host';

$files = dir_list($path);
//exec(sprintf('echo %s > /tmp/preload.log', json_encode($files)));
foreach ($files as $file) {
    if (strpos($file, '.php') && file_exists($file) && is_file($file)) {
        if (opcache_compile_file($file)) {
            echo "Preloading $file success" . PHP_EOL;
        } else {
            echo "Preloading $file failed" . PHP_EOL;
        }
    }
}

function dir_list(string $directory): array
{
    static $array = [];

    $iterator = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::LEAVES_ONLY
    );
    foreach ($iterator as $object) {
        $array[] = $object->getFileName();
    }

    return $array;
}